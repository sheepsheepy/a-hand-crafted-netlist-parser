#include <iostream>
#include "ScalableVectorGraphicsGenerator.hpp"
using namespace std;

void logicgate();
int searchchar(char);
void datasave(int, int, int*, int);
void checksave();
void savelevel();
int gettargetlevel(int);
int getlevel(int, int*, int);
int comparelevel(int, int*, int*);
void savecoordinate();
void drawSVGG();
void setpath(int, int, int, int);
void deletepath();
int getcoordinatex(int);
int getcoordinatey(int);
string buffer;
int gatedata[54][200];									       
int gatenumber[2][10] = { {0,4,9,14,20,26,32,38,44,50},{0,0,0,0,0,0,0,0,0,0} };
int runplace = 0;
vector <int> path;
ScalableVectorGraphicsGenerator SVGG;

int main(int argc, const char * argv[])
{
	while (getline(cin,buffer))
	{
		if (buffer[0] != '#')
		{
			if (buffer[0] == 'I'&& buffer[1] == 'N'&& buffer[2] == 'P'&& buffer[3] == 'U'&& buffer[4] == 'T')
			{
				datasave(searchchar('(') + 1, gatenumber[1][0], gatedata[gatenumber[0][0] + 3], searchchar(')') - 1);
				gatenumber[1][0]++;
			}
			else if (buffer[0] == 'O'&& buffer[1] == 'U'&& buffer[2] == 'T'&& buffer[3] == 'P'&& buffer[4] == 'U' && buffer[5] == 'T')
			{
				datasave(searchchar('(') + 1, gatenumber[1][9], gatedata[gatenumber[0][9] + 3], searchchar(')') - 1);
				gatenumber[1][9]++;
			}
			else if (isdigit(buffer[0]))
				logicgate();
			
		}
	}
	savelevel();
	savecoordinate();
	SVGG.SVGBegin("",1200, 600);
	drawSVGG();
	SVGG.SVGEnd();
	return 0;
}
void logicgate()
{
	int run = 0;
	while (1)
	{
		if (buffer[run] == 'B' && buffer[run + 1] == 'U' && buffer[run + 2] == 'F' && buffer[run + 3] == 'F')
		{
			datasave(0, gatenumber[1][1], gatedata[gatenumber[0][1] + 3], searchchar(' ') - 1);
			datasave(searchchar('(') + 1, gatenumber[1][1], gatedata[gatenumber[0][1] + 4], searchchar(')') - 1);
			gatenumber[1][1]++;
			break;
		}
		else if (buffer[run] == 'N' && buffer[run + 1] == 'O' && buffer[run + 2] == 'T')
		{
			datasave(0, gatenumber[1][2], gatedata[gatenumber[0][2] + 3], searchchar(' ') - 1);
			datasave(searchchar('(') + 1, gatenumber[1][2], gatedata[gatenumber[0][2] + 4], searchchar(')') - 1);
			gatenumber[1][2]++;
			break;
		}
		else if (buffer[run] == 'A' && buffer[run + 1] == 'N' && buffer[run + 2] == 'D')
		{
			datasave(0, gatenumber[1][3], gatedata[gatenumber[0][3] + 3], searchchar(' ') - 1);
			datasave(searchchar('(') + 1, gatenumber[1][3], gatedata[gatenumber[0][3] + 4], searchchar(',') - 1);
			datasave(searchchar(',') + 2, gatenumber[1][3], gatedata[gatenumber[0][3] + 5], searchchar(')') - 1);
			gatenumber[1][3]++;
			break;
		}
		else if (buffer[run] == 'N' && buffer[run + 1] == 'A' && buffer[run + 2] == 'N' && buffer[run + 3] == 'D')
		{
			datasave(0, gatenumber[1][4], gatedata[gatenumber[0][4] + 3], searchchar(' ') - 1);
			datasave(searchchar('(') + 1, gatenumber[1][4], gatedata[gatenumber[0][4] + 4], searchchar(',') - 1);
			datasave(searchchar(',') + 2, gatenumber[1][4], gatedata[gatenumber[0][4] + 5], searchchar(')') - 1);
			gatenumber[1][4]++;
			break;
		}
		else if (buffer[run] == 'O' && buffer[run + 1] == 'R')
		{
			datasave(0, gatenumber[1][5], gatedata[gatenumber[0][5] + 3], searchchar(' ') - 1);
			datasave(searchchar('(') + 1, gatenumber[1][5], gatedata[gatenumber[0][5] + 4], searchchar(',') - 1);
			datasave(searchchar(',') + 2, gatenumber[1][5], gatedata[gatenumber[0][5] + 5], searchchar(')') - 1);
			gatenumber[1][5]++;
			break;
		}
		else if (buffer[run] == 'N' && buffer[run + 1] == 'O' && buffer[run + 2] == 'R')
		{
			datasave(0, gatenumber[1][6], gatedata[gatenumber[0][6] + 3], searchchar(' ') - 1);
			datasave(searchchar('(') + 1, gatenumber[1][6], gatedata[gatenumber[0][6] + 4], searchchar(',') - 1);
			datasave(searchchar(',') + 2, gatenumber[1][6], gatedata[gatenumber[0][6] + 5], searchchar(')') - 1);
			gatenumber[1][6]++;
			break;
		}
		else if (buffer[run] == 'X' && buffer[run + 1] == 'O' && buffer[run + 2] == 'R')
		{
			datasave(0, gatenumber[1][7], gatedata[gatenumber[0][7] + 3], searchchar(' ') - 1);
			datasave(searchchar('(') + 1, gatenumber[1][7], gatedata[gatenumber[0][7] + 4], searchchar(',') - 1);
			datasave(searchchar(',') + 2, gatenumber[1][7], gatedata[gatenumber[0][7] + 5], searchchar(')') - 1);
			gatenumber[1][7]++;
			break;
		}
		else if (buffer[run] == 'N' && buffer[run + 1] == 'X' && buffer[run + 2] == 'O' && buffer[run + 3] == 'R')
		{
			datasave(0, gatenumber[1][8], gatedata[gatenumber[0][8] + 3], searchchar(' ') - 1);
			datasave(searchchar('(') + 1, gatenumber[1][8], gatedata[gatenumber[0][8] + 4], searchchar(',') - 1);
			datasave(searchchar(',') + 2, gatenumber[1][8], gatedata[gatenumber[0][8] + 5], searchchar(')') - 1);
			gatenumber[1][8]++;
			break;
		}
		run++;
	}
}
void datasave(int startplace, int savenumber, int *savearray, int endplace)
{
	while (startplace <= endplace)
	{
		savearray[savenumber] = savearray[savenumber] * 10 + buffer[startplace] - '0';
		startplace++;
	}
}
int searchchar(char targetchar)
{
	int runbuffer = 0;
	while (buffer[runbuffer] != targetchar)
		runbuffer++;
	return runbuffer;
}
void checksave()
{
	for (int i = 0; i < 54; i++)
	{
		for (int j = 0; j < 4; j++)
			cout << gatedata[i][j] << " ";
		cout << endl;
	}
}
void savelevel()
{
	int computenum = 1;
	while (computenum)
	{
		computenum = 0;
		for (int i = 1; i < 9; i++)
		{
			if (comparelevel(gatenumber[1][i], gatedata[gatenumber[0][i]], gatedata[gatenumber[0][i] + 4]))
			{
				computenum = 1;
				break;
			}
			if (i > 2 && i < 9)
			{
				if (comparelevel(gatenumber[1][i], gatedata[gatenumber[0][i]], gatedata[gatenumber[0][i] + 5]))
				{
					computenum = 1;
					break;
				}
			}
			if (comparelevel(gatenumber[1][9], gatedata[gatenumber[0][9]], gatedata[gatenumber[0][9] + 3]))
			{
				computenum = 1;
				break;
			}
		}
	}
	computenum = 1;
	while (computenum)
	{
		computenum = 0;
		for (int i = 0; i < gatenumber[1][9]; i++)
		{
			if (gatedata[gatenumber[0][9]][i] > runplace)
			{
				runplace = gatedata[gatenumber[0][9]][i];
				for (int j = 0; j < gatenumber[1][9]; j++)
					gatedata[gatenumber[0][9]][j] = runplace;
				computenum = 1;
				break;
			}
		}
	}
}
int getlevel(int savenumber, int *savearray, int target)
{
	for (int i = 0; i < savenumber; i++)
	{
		if (savearray[i] == target)
			return i;
	}
	return -1;
}
int gettargetlevel(int target)
{
	for (int i = 0; i < 10; i++)
	{
		if (getlevel(gatenumber[1][i], gatedata[gatenumber[0][i] + 3], target) != -1)//input
			return gatedata[gatenumber[0][i]][getlevel(gatenumber[1][i], gatedata[gatenumber[0][i] + 3], target)];
	}
	return -1;
}
int comparelevel(int savenumber, int *comparearray, int *targetarray)
{

	for (int i = 0; i < savenumber; i++)
	{
		if (comparearray[i] <= gettargetlevel(targetarray[i]))
		{
			comparearray[i] = gettargetlevel(targetarray[i]) + 1;
			return 1;
		}
	}
	return 0;
}
void savecoordinate()
{
	for (int i = 0; i < 10; i++)
	{
		if (i == 0 || i == 9)
			runplace = 100;
		else
			runplace = 80;
		for (int j = 0; j < gatenumber[1][i]; j++)
		{
			gatedata[gatenumber[0][i] + 1][j] = 20 * (gatedata[gatenumber[0][i]][j] * 10 + 1);
			gatedata[gatenumber[0][i] + 2][j] = runplace*(j + 1);
		}
	}
}
void drawSVGG()
{
	
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < gatenumber[1][i]; j++)
		{
			if (i > 0 && i < 9)
			{
				setpath(gatedata[gatenumber[0][i] + 1][j], gatedata[gatenumber[0][i] + 2][j], gatedata[gatenumber[0][i] + 4][j], gatedata[gatenumber[0][i] + 4][j]);
				SVGG.drawPath(path);
				deletepath();
				if (i > 2)
				{
					setpath(gatedata[gatenumber[0][i] + 1][j], gatedata[gatenumber[0][i] + 2][j], gatedata[gatenumber[0][i] + 5][j], gatedata[gatenumber[0][i] + 5][j]);
					SVGG.drawPath(path);
					deletepath();
				}
			}
			for (int k = 0; k < gatenumber[1][9]; k++)
			{
				if (gatedata[gatenumber[0][i] + 3][j] == gatedata[gatenumber[0][9] + 3][k])
				{
					path.push_back(gatedata[gatenumber[0][i] + 1][j] + 15);
					path.push_back(gatedata[gatenumber[0][i] + 2][j] + 30);
					path.push_back(gatedata[gatenumber[0][9] + 1][k] + 15);
					path.push_back(gatedata[gatenumber[0][9] + 2][k] + 30);
					SVGG.drawPath(path);
					deletepath();
				}
			}
			if(i == 0)
				SVGG.drawIOBox(gatedata[gatenumber[0][i] + 1][j], gatedata[gatenumber[0][i] + 2][j], 0);
			else if(i == 1)
				SVGG.drawV(gatedata[gatenumber[0][i] + 1][j], gatedata[gatenumber[0][i] + 2][j]);
			else if(i == 2)
				SVGG.drawINV(gatedata[gatenumber[0][i] + 1][j], gatedata[gatenumber[0][i] + 2][j]);
			else if(i == 3)
				SVGG.drawAND(gatedata[gatenumber[0][i] + 1][j], gatedata[gatenumber[0][i] + 2][j]);
			else if(i == 4)
				SVGG.drawNAND(gatedata[gatenumber[0][i] + 1][j], gatedata[gatenumber[0][i] + 2][j]);
			else if(i == 5)
				SVGG.drawOR(gatedata[gatenumber[0][i] + 1][j], gatedata[gatenumber[0][i] + 2][j]);
			else if(i == 6)
				SVGG.drawNOR(gatedata[gatenumber[0][i] + 1][j], gatedata[gatenumber[0][i] + 2][j]);
			else if(i == 7)
				SVGG.drawXOR(gatedata[gatenumber[0][i] + 1][j], gatedata[gatenumber[0][i] + 2][j]);
			else if(i == 8)
				SVGG.drawNXOR(gatedata[gatenumber[0][i] + 1][j], gatedata[gatenumber[0][i] + 2][j]);
			else 
				SVGG.drawIOBox(gatedata[gatenumber[0][i] + 1][j], gatedata[gatenumber[0][i] + 2][j], 2);

			SVGG.drawText(gatedata[gatenumber[0][i] + 1][j] - 15, gatedata[gatenumber[0][i] + 2][j] + 4, gatedata[gatenumber[0][i] + 3][j]);
		}
	}
}
void setpath(int ax, int ay, int bx, int by)
{
	path.push_back(ax +15);
	path.push_back(ay +30);
	path.push_back(getcoordinatex(bx) +15);
	path.push_back(getcoordinatey(by) +30);
}
void deletepath()
{
	for (int i = 0; i < 4; i++)
		path.pop_back();
}
int getcoordinatex(int target)
{
	for (int i = 0; i < 10; i++)
	{
		if (getlevel(gatenumber[1][i], gatedata[gatenumber[0][i] + 3], target) != -1)//input
			return gatedata[gatenumber[0][i] + 1][getlevel(gatenumber[1][i], gatedata[gatenumber[0][i] + 3], target)];
	}
	return -1;
}
int getcoordinatey(int target)
{
	for (int i = 0; i < 10; i++)
	{
		if (getlevel(gatenumber[1][i], gatedata[gatenumber[0][i] + 3], target) != -1)//input
			return gatedata[gatenumber[0][i] + 2][getlevel(gatenumber[1][i], gatedata[gatenumber[0][i] + 3], target)];
	}
	return -1;
}
